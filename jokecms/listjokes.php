<?php

try {
	$pdo = new PDO ('mysql:host=localhost;dbname=ijdb', 'root', 'root');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e) {
	$output = 'Unable to connect to the database server: ' .$e->getMessage();
	include 'output.html.php';
	exit();
}

try {
	$sql = 'SELECT joketext FROM joke';
	$result = $pdo->query($sql);
}
catch (PDOException $e) {
	$error = 'Erro fetching kokes: ' . $e->getMessage();
	include 'error.html.php';
	exit();
}

while ($row = $result->fetch()) {
	$jokes[] = $row['joketext'];
}

include 'jokes.html.php';

?>